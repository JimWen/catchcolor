#include <Windows.h>
#include <WindowsX.h>

#include "resource.h"
#include "SettingOtherProc.h"
#include "config.h"
#include "hotkey.h"

BOOL CALLBACK SettingOtherProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch(message)
	{
	case WM_INITDIALOG:
		{
			//初始化界面
			Button_SetCheck(GetDlgItem(hDlg, IDCK_AUTORUN), GetConfig(TEXT("IsAutoRun")) ? BST_CHECKED : BST_UNCHECKED);
			Button_SetCheck(GetDlgItem(hDlg, IDCK_HIDE), GetConfig(TEXT("IsHide")) ? BST_CHECKED : BST_UNCHECKED);

			ShowHotkey(GetDlgItem(hDlg, IDHT_SETTING_DLG), GetConfig(TEXT("SettingModifier")), GetConfig(TEXT("SettingKey")));
		}
		return (TRUE);

	//对话框和控件背景设为白色
	case WM_CTLCOLORDLG:
	case WM_CTLCOLORBTN:
	case WM_CTLCOLORSTATIC:
		return GetStockObject(WHITE_BRUSH);
	}

	return (FALSE);
}