#include <Windows.h>
#include <WindowsX.h>
#include <commctrl.h>

#include "resource.h"
#include "SettingHistoryProc.h"
#include "config.h"

BOOL CALLBACK SettingHistoryProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	static OPENFILENAME ofn;
	static TCHAR		szFileName[MAX_PATH], szTitleName[MAX_PATH];
	static TCHAR		szFilter[] = TEXT("Color Files(*.clr)\0*.clr\0")
									 TEXT("All Files(*.*)\0*.*\0\0");

	TCHAR szClr[20];
	COLORNODE clrNode;

	switch(message)
	{
	case WM_INITDIALOG:
		{
			//设置R、G、B范围
			SendMessage(GetDlgItem(hDlg, IDSL_R), TBM_SETRANGE, TRUE, (LPARAM)MAKELONG(0, 255));
			SendMessage(GetDlgItem(hDlg, IDSL_G), TBM_SETRANGE, TRUE, (LPARAM)MAKELONG(0, 255));
			SendMessage(GetDlgItem(hDlg, IDSL_B), TBM_SETRANGE, TRUE, (LPARAM)MAKELONG(0, 255));

			//打开文件和保存文件设置
			ofn.lStructSize			= sizeof(OPENFILENAME);			//结构体大小
			ofn.hwndOwner			= hDlg;							//父窗口句柄
			ofn.hInstance			= NULL;
			ofn.lpstrFilter			= szFilter;						//文件后缀名过滤选项
			ofn.lpstrCustomFilter	= NULL;
			ofn.nMaxCustFilter		= 0;
			ofn.nFilterIndex		= 0;
			ofn.lpstrFile			= szFileName;					//打开文件全路径保存位置
			ofn.nMaxFile			= MAX_PATH;						//打开文件全路径长度
			ofn.lpstrFileTitle		= szTitleName;					//打开文件名保存位置
			ofn.nMaxFileTitle		= MAX_PATH;						//打开文件名长度
			ofn.lpstrInitialDir		= NULL;							//打开的初始文件夹
			ofn.lpstrTitle			= NULL;							//打开的文件对话框标题
			ofn.Flags				= 0;
			ofn.nFileOffset			= 0;
			ofn.nFileExtension		= 0;
			ofn.lpstrDefExt			= TEXT("clr");					//默认的文件后缀
			ofn.lCustData			= 0;
			ofn.lpfnHook			= NULL;
			ofn.lpTemplateName		= NULL;
		}
		return (TRUE);

	//更改背景颜色
	case WM_CTLCOLORDLG:
	case WM_CTLCOLORBTN:
	case WM_CTLCOLORSTATIC:
		return GetStockObject(WHITE_BRUSH);

	case WM_CLOSE:
		{
			EndDialog(hDlg,0);
		}
		return (TRUE);

	case WM_VSCROLL:
		//获得滚动条颜色值
		clrNode = GetSliderClrNode(hDlg);

		//设置当前颜色值
		GetClrNodeText(clrNode, szClr);
		SetWindowText(GetDlgItem(hDlg, IDS_CUR_COLOR), szClr);

		//显示当前颜色
		ShowCurColor(hDlg, RGB(clrNode.byteRed, clrNode.byteGreen, clrNode.byteBlue));
		return (TRUE);

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		//List Box通知消息 
		case IDC_COLOR_LIST:
			//获得当前List选项对应的颜色值
			clrNode = GetClrNode(ListBox_GetCurSel(GetDlgItem(hDlg, IDC_COLOR_LIST)));

			//单及显示对应颜色
			if (LBN_SELCHANGE == HIWORD(wParam))
			{
				//设置当前颜色值
				GetClrNodeText(clrNode, szClr);
				SetWindowText(GetDlgItem(hDlg, IDS_CUR_COLOR), szClr);

				//设置当前Slider的位置
				SendMessage(GetDlgItem(hDlg, IDSL_R), TBM_SETPOS, TRUE, clrNode.byteRed);
				SendMessage(GetDlgItem(hDlg, IDSL_G), TBM_SETPOS, TRUE, clrNode.byteGreen);
				SendMessage(GetDlgItem(hDlg, IDSL_B), TBM_SETPOS, TRUE, clrNode.byteBlue);

				//显示当前颜色
				ShowCurColor(hDlg, RGB(clrNode.byteRed, clrNode.byteGreen, clrNode.byteBlue));
			}

			//双击复制颜色
			if(LBN_DBLCLK == HIWORD(wParam))
			{
				GetClrNodeText(clrNode, szClr);
				CopyClrToClipBoard(hDlg, szClr);
			}
			return TRUE;

		//导入
		case IDB_IMPORT:
			ofn.lpstrTitle = TEXT("选择要导入的取色历史文件");
			if (GetOpenFileName(&ofn))
			{
				ImportColorHistory(szFileName, GetDlgItem(hDlg, IDC_COLOR_LIST));
			}
			return TRUE;

		//导出
		case IDB_EXPORT:
			ofn.lpstrTitle = TEXT("设置要导出的取色历史文件路径和文件名");
			if (GetSaveFileName(&ofn))
			{
				ExportColorHistory(szFileName);
			}
			return TRUE;

		//清空历史
		case IDB_CLEAR:
			{
				ClearClrList(GetDlgItem(hDlg, IDC_COLOR_LIST));
			}
			return (TRUE);

		//复制调整后的颜色值
		case IDB_COPY:
			//获得滚动条颜色值
			clrNode = GetSliderClrNode(hDlg);

			//复制当前颜色值
			GetClrNodeText(clrNode, szClr);
			CopyClrToClipBoard(hDlg, szClr);
			return (TRUE);
		}
		return (FALSE);
	}
	return (FALSE);
}

/************************************************************************/
/* 显示当前的颜色值                                                     */
/************************************************************************/
void ShowCurColor(HWND hDlg, COLORREF clr)
{
	HBRUSH hBrush;
	RECT rect;
	HDC hdc;

	GetClientRect(GetDlgItem(hDlg, IDS_VIEW_COLOR), &rect);

	hBrush = CreateSolidBrush(clr);

	hdc = GetDC(GetDlgItem(hDlg, IDS_VIEW_COLOR));
	FillRect(hdc, &rect, hBrush);
	ReleaseDC(GetDlgItem(hDlg, IDS_VIEW_COLOR), hdc);

	DeleteObject(hBrush);
}

/************************************************************************/
/* 获取当前滚动条的颜色值                                               */
/************************************************************************/
COLORNODE GetSliderClrNode(HWND hDlg)
{
	COLORNODE clrNode;

	clrNode.clrType = GetConfig(TEXT("ColorType"));
	clrNode.byteRed = (BYTE)SendMessage(GetDlgItem(hDlg, IDSL_R), TBM_GETPOS, 0, 0);
	clrNode.byteGreen = (BYTE)SendMessage(GetDlgItem(hDlg, IDSL_G), TBM_GETPOS, 0, 0);
	clrNode.byteBlue = (BYTE)SendMessage(GetDlgItem(hDlg, IDSL_B), TBM_GETPOS, 0, 0);

	return clrNode;
}