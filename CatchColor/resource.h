//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by CatchColor.rc
//
#define IDB_TAKECOLOR                   3
#define IDB_CLEAR                       3
#define IDB_EXIT                        4
#define IDB_COPY                        4
#define IDB_EXPORT                      5
#define IDB_ABOUT                       5
#define IDD_COLOR                       101
#define IDD_SET                         102
#define IDD_SETTING                     102
#define IDD_SETTING_CATCH               103
#define IDD_SETTING_HISTORY             104
#define IDI_MAINICON                    104
#define IDD_SETTING_OTHER               105
#define IDR_MAINMENU                    105
#define IDD_ABOUT                       106
#define IDP_VIEW1                       1001
#define IDP_VIEW2                       1002
#define IDC_VIEW2                       1003
#define IDC_VIEW1                       1004
#define IDCK_AUTORUN                    1005
#define IDC_COLORTYPE                   1005
#define IDHT_SETTING_DLG                1006
#define IDCK_HIDE                       1007
#define IDC_TAB                         1008
#define IDC_COLOR_LIST                  1009
#define IDCK_MOVE                       1010
#define IDCK_FORGROUND                  1011
#define IDHT_CATCH_COLOR                1012
#define IDSL_R                          1014
#define IDSL_G                          1015
#define IDSL_B                          1016
#define IDB_IMPORT                      1017
#define IDS_COLOR                       1018
#define IDS_POS                         1019
#define IDS_VIEW_COLOR                  1020
#define IDS_CUR_COLOR                   1021
#define IDB_SYSLINK                     1023
#define ID_40001                        40001
#define ID_40002                        40002
#define IDB_SETTING                     40003
#define IDP_COLOR                       -1
#define IDS_DLG_CONTAINER               -1
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        106
#define _APS_NEXT_COMMAND_VALUE         40004
#define _APS_NEXT_CONTROL_VALUE         1024
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
