#ifndef COLOR_DLG_PROC_H_H
#define COLOR_DLG_PROC_H_H

BOOL CALLBACK  ColorDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
void SetWindowToSuspend(HWND hwnd);
void HideModelDlg(HWND hDlg);
void ShowModelDlg(HWND hDlg);

#define UPDATE_COLOR_TIMER 1	//定时更新定时器
#define CURSOR_WINDOW_DIS_X	20	//鼠标和对应跟随窗口间的X间距
#define CURSOR_WINDOW_DIS_Y	20  //鼠标和对应跟随窗口间的Y间距

#endif