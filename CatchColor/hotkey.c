#include<Windows.h>
#include <commctrl.h>
#include <imm.h>

#include "hotkey.h"

WORD g_hotkeyClrId;
WORD g_hotkeySettingId;

/************************************************************************/
/* 设置热键                                                             */
/************************************************************************/
INT SetHotkey(HWND hwnd, UINT nModifier, UINT nKey, PTSTR szAtomName)
{
	WORD hotkeyId;
	
	hotkeyId = GlobalAddAtom(szAtomName);
	if (FALSE == RegisterHotKey(hwnd, hotkeyId, nModifier, nKey))
	{
		return -1;
	}

	return hotkeyId;
}

/************************************************************************/
/* 取消设置热键                                                         */
/************************************************************************/
BOOL CancelHotkey(HWND hwnd, WORD hotkeyId)
{
	return UnregisterHotKey(hwnd, hotkeyId);
}

/************************************************************************/
/* 获得指定热键控件的值,由于热键控件的HOTKEYF_ALT和
** 设置热键函数MOD_ALT不一样，故要做一个转换*/
/************************************************************************/
void GetHotkey(HWND hHotkey, UINT *nModifier, UINT *nKey)
{
	WORD wHotkey;
	UINT nTempModifier;

	wHotkey = (WORD) SendMessage(hHotkey, HKM_GETHOTKEY, 0, 0);

	//分拆出热键
	*nKey = LOBYTE(wHotkey);
	nTempModifier = HIBYTE(wHotkey);

	*nModifier=0;
	if(nTempModifier & HOTKEYF_ALT)
	{
		*nModifier |= MOD_ALT;
	}
	if(nTempModifier & HOTKEYF_CONTROL)
	{
		*nModifier |= MOD_CONTROL;
	}
	if(nTempModifier & HOTKEYF_SHIFT)
	{
		*nModifier |= MOD_SHIFT;
	}
}

/************************************************************************/
/* 设置指定热键控件的显示                                               */
/************************************************************************/
void ShowHotkey(HWND hHotkey, UINT nModifier, UINT nKey)
{
	UINT nTempModifier;

	nTempModifier = 0;
	if(nModifier & MOD_ALT)
	{
		nTempModifier |= HOTKEYF_ALT;
	}
	if(nModifier & MOD_CONTROL)
	{
		nTempModifier |= HOTKEYF_CONTROL;
	}
	if(nModifier & MOD_SHIFT)
	{
		nTempModifier |= HOTKEYF_SHIFT;
	}

	//设置热键
	SendMessage(hHotkey,
				HKM_SETHOTKEY ,
				MAKEWPARAM(MAKEWORD((BYTE)nKey, (BYTE)nTempModifier) ,0),
				0);
}