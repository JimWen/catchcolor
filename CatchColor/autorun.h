#ifndef AUTORUN_H_H_H
#define AUTORUN_H_H_H

BOOL SetAutoRun(LPCTSTR szRegName, LPCTSTR szExePath);
BOOL CancelAutoRun(LPCTSTR szRegName);

#endif