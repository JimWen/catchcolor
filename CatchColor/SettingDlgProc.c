#include <Windows.h>
#include <WindowsX.h>
#include <commctrl.h>
#include <tchar.h>

#include "resource.h"
#include "config.h"
#include "trayicon.h"
#include "hotkey.h"
#include "colorOperate.h"
#include "autorun.h"

#include "SettingDlgProc.h"
#include "ColorDlgProc.h"
#include "SettingCatchProc.h"
#include "SettingHistoryProc.h"
#include "SettingOtherProc.h"
#include "AboutDlgProc.h"

#pragma comment(lib,"ComCtl32.lib")

static HWND hwndSettingCatch;
static HWND hwndSettingHistory;
static HWND hwndSettingOther;

BOOL CALLBACK SettingDlgProc (HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	static BOOL bIsShowColorDlg=FALSE;				//是否已经显示取色对话框

	COLORREF clr;
	TCITEM tabItem;
	LPNMHDR lpnmhdr;
	POINT pt;
	HMENU hMenu;
	UINT nKey, nModifier;
	TCHAR szInt[10];
	UINT cxScreen, cyScreen;
	RECT rectWnd;
	HDC hdcScreen;
	TCHAR szClr[20];
	HGLOBAL hGlobal = NULL;

	switch(message)
	{
	case WM_INITDIALOG:
		//设置主窗口图标
		SendMessage(hDlg, WM_SETICON, (WPARAM)ICON_SMALL, (LPARAM)LoadIcon((HINSTANCE)GetWindowLong(hDlg, GWL_HINSTANCE), MAKEINTRESOURCE(IDI_MAINICON)));
		SendMessage(hDlg, WM_SETICON, (WPARAM)ICON_BIG, (LPARAM)LoadIcon((HINSTANCE)GetWindowLong(hDlg, GWL_HINSTANCE), MAKEINTRESOURCE(IDI_MAINICON)));

		//创建Tab选项
		tabItem.mask = TCIF_TEXT;

		tabItem.pszText = TEXT("取色设置");
		tabItem.cchTextMax = sizeof(TEXT("取色设置"));
		TabCtrl_InsertItem(GetDlgItem(hDlg, IDC_TAB), 0, &tabItem);

		tabItem.pszText = TEXT("其他设置");
		tabItem.cchTextMax = sizeof(TEXT("其他设置"));
		TabCtrl_InsertItem(GetDlgItem(hDlg, IDC_TAB), 1, &tabItem);

		tabItem.pszText = TEXT("取色历史");
		tabItem.cchTextMax = sizeof(TEXT("取色历史"));
		TabCtrl_InsertItem(GetDlgItem(hDlg, IDC_TAB), 2, &tabItem);

		//设置Tab默认选项
		TabCtrl_SetCurSel(GetDlgItem(hDlg, IDC_TAB), 0);

		//创建和设置默认显示子对话框
		DownDlgCreate(hDlg);
		DownDlgSelect(0);

		//设置窗口默认位置在屏幕中间
		GetWindowRect(hDlg, &rectWnd);
		cxScreen = GetSystemMetrics(SM_CXSCREEN);
		cyScreen = GetSystemMetrics(SM_CYSCREEN);

		SetWindowPos(hDlg, NULL, 
					(cxScreen+rectWnd.left-rectWnd.right)/2, 
					(cyScreen-rectWnd.bottom+rectWnd.top)/2, 
					0, 0, 
					SWP_NOZORDER | SWP_NOSIZE);
		return (TRUE);

	case WM_NOTIFY:
		lpnmhdr = (LPNMHDR) lParam;

		//处理Tab切换时的子窗口切换
		if (lpnmhdr->code == TCN_SELCHANGE)
		{
			DownDlgSelect(TabCtrl_GetCurSel(lpnmhdr->hwndFrom));
		}
		return TRUE;

	//托盘消息处理
	case WM_TRAYMESSAGE:
		switch (lParam)
		{
			//双击左键显示主窗口
		case WM_LBUTTONDBLCLK:
			SetForegroundWindow(hDlg);
			ShowWindow(hDlg, SW_NORMAL) ;
			return TRUE ;

			//右键弹出菜单
		case WM_RBUTTONUP:
			GetCursorPos(&pt) ;
			hMenu = LoadMenu((HINSTANCE)GetWindowLong(hDlg, GWL_HINSTANCE), MAKEINTRESOURCE(IDR_MAINMENU));
			hMenu = GetSubMenu(hMenu, 0);
			TrackPopupMenu(hMenu, TPM_LEFTBUTTON, pt.x, pt.y, 0, hDlg, NULL) ;
			return TRUE ;
		}

		return FALSE;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		//注意非模态对话框关闭调用DestroyWindow
		case IDB_EXIT:
			DestroyWindow(hwndSettingOther);
			DestroyWindow(hwndSettingHistory);
			DestroyWindow(hwndSettingCatch);
			DestroyWindow(hDlg);
			return (TRUE);

		case IDCANCEL:
            {
				ShowWindow(hDlg, SW_HIDE);
            }
            return (TRUE);

		case IDB_SETTING:
			SetForegroundWindow(hDlg);
			ShowWindow(hDlg, SW_NORMAL);
			return (TRUE);

		case IDOK:
            {
				/************************************************************************/
				/*读取当前所有参数并同步到配置文件中		                            */
				/************************************************************************/
				ComboBox_GetText(GetDlgItem(hwndSettingCatch, IDC_VIEW1), szInt, 10);
				SetConfig(g_szConfigPath,
							TEXT("View1Ratio"),
							_ttoi(szInt),
							TRUE);

				ComboBox_GetText(GetDlgItem(hwndSettingCatch, IDC_VIEW2), szInt, 10);
				SetConfig(g_szConfigPath,
							TEXT("View2Ratio"),
							_ttoi(szInt),
							TRUE);

				SetConfig(g_szConfigPath,
							TEXT("ColorType"),
							ComboBox_GetCurSel(GetDlgItem(hwndSettingCatch, IDC_COLORTYPE)),
							TRUE);

				SetConfig(g_szConfigPath, 
							TEXT("IsMove"), 
							BST_CHECKED == Button_GetCheck(GetDlgItem(hwndSettingCatch, IDCK_MOVE)) ? 1 : 0,
							TRUE);

				SetConfig(g_szConfigPath, 
							TEXT("IsForground"), 
							BST_CHECKED == Button_GetCheck(GetDlgItem(hwndSettingCatch, IDCK_FORGROUND)) ? 1 : 0,
							TRUE);

				GetHotkey(GetDlgItem(hwndSettingCatch, IDHT_CATCH_COLOR), &nModifier, &nKey);

				SetConfig(g_szConfigPath, 
							TEXT("ClrKey"), 
							nKey,
							TRUE);

				SetConfig(g_szConfigPath, 
							TEXT("ClrModifier"), 
							nModifier,
							TRUE);

				SetConfig(g_szConfigPath, 
							TEXT("IsAutoRun"), 
							BST_CHECKED == Button_GetCheck(GetDlgItem(hwndSettingOther, IDCK_AUTORUN)) ? 1 : 0,
							TRUE);

				SetConfig(g_szConfigPath, 
							TEXT("IsHide"), 
							BST_CHECKED == Button_GetCheck(GetDlgItem(hwndSettingOther, IDCK_HIDE)) ? 1 : 0,
							TRUE);

				GetHotkey(GetDlgItem(hwndSettingOther, IDHT_SETTING_DLG), &nModifier, &nKey);

				SetConfig(g_szConfigPath, 
							TEXT("SettingKey"), 
							nKey,
							TRUE);

				SetConfig(g_szConfigPath, 
							TEXT("SettingModifier"),
							nModifier,
							TRUE);

				/************************************************************************/
				/*根据新参数调整自动启动                                                */
				/************************************************************************/
				if (TRUE == GetConfig(TEXT("IsAutoRun")))
				{
					SetAutoRun(TEXT("JimWen-CatchColor"), g_szExePath);
				}
				else
				{
					CancelAutoRun(TEXT("JimWen-CatchColor"));
				}

				/************************************************************************/
				/*根据新参数调整后台运行                                                */
				/************************************************************************/
				if (TRUE == GetConfig(TEXT("IsHide")))
				{
					DeleteTray(hDlg);
					
				}
				else
				{
					ToTray(hDlg, IDI_MAINICON);
				}
				//ShowWindow(hDlg, SW_HIDE);//设置完成后隐藏窗口

				/************************************************************************/
				/*根据新参数调整取色热键和设置窗口热键                                  */
				/************************************************************************/
				CancelHotkey(hDlg, g_hotkeyClrId);
				CancelHotkey(hDlg, g_hotkeySettingId);
				g_hotkeyClrId =  SetHotkey(hDlg, 
											GetConfig(TEXT("ClrModifier")), 
											GetConfig(TEXT("ClrKey")), 
											TEXT("JimWen-ClrHotkey"));
				g_hotkeySettingId =  SetHotkey(hDlg, 
											GetConfig(TEXT("SettingModifier")), 
											GetConfig(TEXT("SettingKey")), 
											TEXT("JimWen-SettingHotkey"));

				if (-1 == g_hotkeyClrId)
				{
					MessageBox(hDlg, TEXT("取色热键组合已被占用，请重新设置"), TEXT("警告"), MB_OK | MB_ICONWARNING);
					return (TRUE);
				}
				if (-1 == g_hotkeySettingId)
				{
					MessageBox(hDlg, TEXT("设置热键组合已被占用，请重新设置"), TEXT("警告"), MB_OK | MB_ICONWARNING);
					return (TRUE);
				}

				MessageBox(hDlg, TEXT("更改设置成功!"), TEXT("通知"), MB_OK | MB_ICONINFORMATION);
            }
			return (TRUE);

		//第一次调用时弹出取色框，第二次调用即开始取色
		case IDB_TAKECOLOR:
			if (FALSE == bIsShowColorDlg)
			{
				bIsShowColorDlg = TRUE;
				DialogBox((HINSTANCE)GetWindowLong(hDlg, GWL_HINSTANCE), MAKEINTRESOURCE(IDD_COLOR), NULL, ColorDlgProc);
				bIsShowColorDlg = FALSE;
			}
			else
			{
				//获得当前鼠标位置
				GetCursorPos(&pt);

				//获得当前鼠标位置的RGB值
				hdcScreen = GetDC(NULL);
				clr = GetPixel(hdcScreen, pt.x, pt.y);
				ReleaseDC(NULL, hdcScreen);

				//颜色值复制到剪切板上
				GetClrTypeAndRGBText(GetConfig(TEXT("ColorType")), clr, szClr);
				CopyClrToClipBoard(hDlg, szClr);
				
				//记录下取色
				PushClrTypeAndRGB(GetConfig(TEXT("ColorType")), clr, GetDlgItem(hwndSettingHistory, IDC_COLOR_LIST));
			}
			return (TRUE);

		//关于...
		case IDB_ABOUT:
			DialogBox((HINSTANCE)GetWindowLong(hDlg, GWL_HINSTANCE), MAKEINTRESOURCE(IDD_ABOUT), hDlg, AboutDlgProc);
			return (TRUE);
		}
		return (FALSE);

	//热键消息
	case WM_HOTKEY:
		if (wParam == g_hotkeySettingId)//显示设置主窗口
		{
			SetForegroundWindow(hDlg);
			ShowWindow(hDlg, SW_NORMAL) ;
		}
		else if (wParam == g_hotkeyClrId)//显示取色对话框
		{
			SendMessage(hDlg, WM_COMMAND, MAKEWPARAM(IDB_TAKECOLOR, 0), 0);
		}
		return (TRUE);

	case WM_DESTROY:
		PostQuitMessage(0);
		return (TRUE);
	}

	return (FALSE);
}

/**
 *功能:创建对话框的tab下几个子窗口
 *参数:hDlg--对话框句柄
 *返回:无
 *其他:2014/04/01 By Jim Wen Ver1.0
**/
void DownDlgCreate(HWND hDlg)
{
	hwndSettingCatch = CreateDialog((HINSTANCE)GetWindowLong(hDlg, GWL_HINSTANCE), 
									MAKEINTRESOURCE(IDD_SETTING_CATCH), 
									GetDlgItem(hDlg, IDS_DLG_CONTAINER), 
									SettingCatchProc);
	hwndSettingOther = CreateDialog((HINSTANCE)GetWindowLong(hDlg, GWL_HINSTANCE), 
									MAKEINTRESOURCE(IDD_SETTING_OTHER), 
									GetDlgItem(hDlg, IDS_DLG_CONTAINER), 
									SettingOtherProc);
	hwndSettingHistory = CreateDialog((HINSTANCE)GetWindowLong(hDlg, GWL_HINSTANCE), 
									MAKEINTRESOURCE(IDD_SETTING_HISTORY), 
									GetDlgItem(hDlg, IDS_DLG_CONTAINER), 
									SettingHistoryProc);
}

/**
 *功能:切换到对应索引的子窗口
 *参数:nIndex--以0为起始的子窗口索引
 *返回:无
 *其他:2014/04/01 By Jim Wen Ver1.0
**/
void DownDlgSelect(UINT nIndex)
{
	ShowWindow(hwndSettingCatch, nIndex == 0 ? SW_NORMAL : SW_HIDE);
	ShowWindow(hwndSettingOther, nIndex == 1 ? SW_NORMAL : SW_HIDE);
	ShowWindow(hwndSettingHistory, nIndex == 2 ? SW_NORMAL : SW_HIDE);
}