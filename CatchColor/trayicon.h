#ifndef TRAYICON_H_H_H
#define TRAYICON_H_H_H


#define WM_TRAYMESSAGE WM_USER+25

BOOL ToTray(const HWND hCtrl, const UINT uIconID) ;
BOOL DeleteTray(const HWND hCtrl) ;												

#endif