#include <Windows.h>
#include <stdio.h>
#include <WindowsX.h>
#include <tchar.h>
#include "config.h"

//软件配置文件
CONFIGDATA g_configData;

//工作路径
TCHAR g_szExePath[MAX_PATH] ;						//程序本身路径
TCHAR g_szConfigPath[MAX_PATH] ;					//软件配置文件路径

/**
 *功能:从软件配置文件中读取参数到当前的对应参数中
 *参数:szConfigFile--参数文件路径
 *返回:无
 *其他:2014/04/01 By Jim Wen Ver1.0
**/
void ReadConfig(LPCTSTR szConfigFile)
{
	g_configData.nView1Ratio	= GetPrivateProfileInt(TEXT("CatchColor"), TEXT("View1Ratio"), 3, szConfigFile);
	g_configData.nView2Ratio	= GetPrivateProfileInt(TEXT("CatchColor"), TEXT("View2Ratio"), 9, szConfigFile);
	g_configData.nColorType		= (COLORTYPE)GetPrivateProfileInt(TEXT("CatchColor"), TEXT("ColorType"), 0, szConfigFile);
	g_configData.bIsMove		= GetPrivateProfileInt(TEXT("CatchColor"), TEXT("IsMove"), 1, szConfigFile);
	g_configData.bIsForground	= GetPrivateProfileInt(TEXT("CatchColor"), TEXT("IsForground"), 1, szConfigFile);
	g_configData.nClrKey		= GetPrivateProfileInt(TEXT("CatchColor"), TEXT("ClrKey"), 84, szConfigFile);
	g_configData.nClrModifier	= GetPrivateProfileInt(TEXT("CatchColor"), TEXT("ClrModifier"), 1, szConfigFile);

	g_configData.bIsAutoRun			= GetPrivateProfileInt(TEXT("CatchColor"), TEXT("IsAutoRun"), 1, szConfigFile);
	g_configData.bIsHide			= GetPrivateProfileInt(TEXT("CatchColor"), TEXT("IsHide"), 1, szConfigFile);
	g_configData.nSettingKey		= GetPrivateProfileInt(TEXT("CatchColor"), TEXT("SettingKey"), 84, szConfigFile);
	g_configData.nSettingModifier	= GetPrivateProfileInt(TEXT("CatchColor"), TEXT("SettingModifier"), 1, szConfigFile);
}

/**
 *功能:将当前的全部软件设置保存到配置文件中
 *参数:szConfigFile--参数文件路径
 *返回:保存成功返回TRUE,否则返回FALSE
 *其他:2014/04/01 By Jim Wen Ver1.0
**/
BOOL SaveConfig(LPCTSTR szConfigFile)
{
	TCHAR szInt[10];

	_itot(g_configData.nView1Ratio, szInt, 10);
	if (FALSE == WritePrivateProfileString(TEXT("CatchColor"), TEXT("View1Ratio"), szInt, szConfigFile))
	{
		return FALSE;
	}
	_itot(g_configData.nView2Ratio, szInt, 10);
	if (FALSE == WritePrivateProfileString(TEXT("CatchColor"), TEXT("View2Ratio"), szInt, szConfigFile))
	{
		return FALSE;
	}
	_itot(g_configData.nColorType, szInt, 10);
	if (FALSE == WritePrivateProfileString(TEXT("CatchColor"), TEXT("ColorType"), szInt, szConfigFile))
	{
		return FALSE;
	}
	_itot(g_configData.bIsMove, szInt, 10);
	if (FALSE == WritePrivateProfileString(TEXT("CatchColor"), TEXT("IsMove"), szInt, szConfigFile))
	{
		return FALSE;
	}
	_itot(g_configData.bIsForground, szInt, 10);
	if (FALSE == WritePrivateProfileString(TEXT("CatchColor"), TEXT("IsForground"), szInt, szConfigFile))
	{
		return FALSE;
	}
	_itot(g_configData.nClrKey, szInt, 10);
	if (FALSE == WritePrivateProfileString(TEXT("CatchColor"), TEXT("ClrKey"), szInt, szConfigFile))
	{
		return FALSE;
	}
	_itot(g_configData.nClrModifier, szInt, 10);
	if (FALSE == WritePrivateProfileString(TEXT("CatchColor"), TEXT("ClrModifier"), szInt, szConfigFile))
	{
		return FALSE;
	}

	_itot(g_configData.bIsAutoRun, szInt, 10);
	if (FALSE == WritePrivateProfileString(TEXT("CatchColor"), TEXT("IsAutoRun"), szInt, szConfigFile))
	{
		return FALSE;
	}
	_itot(g_configData.bIsHide, szInt, 10);
	if (FALSE == WritePrivateProfileString(TEXT("CatchColor"), TEXT("IsHide"), szInt, szConfigFile))
	{
		return FALSE;
	}
	_itot(g_configData.nSettingKey, szInt, 10);
	if (FALSE == WritePrivateProfileString(TEXT("CatchColor"), TEXT("SettingKey"), szInt, szConfigFile))
	{
		return FALSE;
	}
	_itot(g_configData.nSettingModifier, szInt, 10);
	if (FALSE == WritePrivateProfileString(TEXT("CatchColor"), TEXT("SettingModifier"), szInt, szConfigFile))
	{
		return FALSE;
	}

	return TRUE;
}

/**
 *功能:设计设置当前的软件参数，可选择是否更新配置文件中参数
 *参数:szConfigFile--参数文件路径
 *	   szConfigName--参数名称
 *	   nConfigValue--参数值
 *	   bSaveToFile--是否保存到配置文件中
 *返回:设置成功返回TRUE,否则返回FALSE
 *其他:2014/04/01 By Jim Wen Ver1.0
**/
BOOL SetConfig(LPCTSTR szConfigFile, LPCTSTR szConfigName, UINT nConfigValue, BOOL bSaveToFile)
{
	TCHAR szInt[10];

	//更新内存数据
	if (lstrcmp(szConfigName, TEXT("View1Ratio")) == 0)
	{
		g_configData.nView1Ratio = nConfigValue;
	}
	else if (lstrcmp(szConfigName, TEXT("View2Ratio")) == 0)
	{
		g_configData.nView2Ratio = nConfigValue;
	}
	else if (lstrcmp(szConfigName, TEXT("ColorType")) == 0)
	{
		g_configData.nColorType = nConfigValue;
	}
	else if (lstrcmp(szConfigName, TEXT("IsMove")) == 0)
	{
		g_configData.bIsMove = nConfigValue;
	}
	else if (lstrcmp(szConfigName, TEXT("IsForground")) == 0)
	{
		g_configData.bIsForground = nConfigValue;
	}
	else if (lstrcmp(szConfigName, TEXT("ClrKey")) == 0)
	{
		g_configData.nClrKey = nConfigValue;
	}
	else if (lstrcmp(szConfigName, TEXT("ClrModifier")) == 0)
	{
		g_configData.nClrModifier = nConfigValue;
	}
	else if (lstrcmp(szConfigName, TEXT("IsAutoRun")) == 0)
	{
		g_configData.bIsAutoRun = nConfigValue;
	}
	else if (lstrcmp(szConfigName, TEXT("IsHide")) == 0)
	{
		g_configData.bIsHide = nConfigValue;
	}
	else if (lstrcmp(szConfigName, TEXT("SettingKey")) == 0)
	{
		g_configData.nSettingKey = nConfigValue;
	}
	else if (lstrcmp(szConfigName,TEXT("SettingModifier")) == 0)
	{
		g_configData.nSettingModifier = nConfigValue;
	}

	//更新配置文件参数
	if (TRUE == bSaveToFile)
	{
		_itot(nConfigValue, szInt, 10);
		return WritePrivateProfileString(TEXT("CatchColor"), szConfigName, szInt, szConfigFile);
	}

	return TRUE;
}

/**
 *功能:读取设置当前的软件参数
 *参数:szConfigName--参数名称
 *返回:对应的参数
 *其他:2014/04/01 By Jim Wen Ver1.0
**/
UINT GetConfig(LPCTSTR szConfigName)
{
	//读取内存数据
	if (lstrcmp(szConfigName, TEXT("View1Ratio")) == 0)
	{
		return g_configData.nView1Ratio;
	}
	else if (lstrcmp(szConfigName, TEXT("View2Ratio")) == 0)
	{
		return g_configData.nView2Ratio;
	}
	else if (lstrcmp(szConfigName, TEXT("ColorType")) == 0)
	{
		return g_configData.nColorType;
	}
	else if (lstrcmp(szConfigName, TEXT("IsMove")) == 0)
	{
		return g_configData.bIsMove;
	}
	else if (lstrcmp(szConfigName, TEXT("IsForground")) == 0)
	{
		return g_configData.bIsForground;
	}
	else if (lstrcmp(szConfigName, TEXT("ClrKey")) == 0)
	{
		return g_configData.nClrKey;
	}
	else if (lstrcmp(szConfigName, TEXT("ClrModifier")) == 0)
	{
		return g_configData.nClrModifier;
	}
	else if (lstrcmp(szConfigName, TEXT("IsAutoRun")) == 0)
	{
		return g_configData.bIsAutoRun ;
	}
	else if (lstrcmp(szConfigName, TEXT("IsHide")) == 0)
	{
		return g_configData.bIsHide;
	}
	else if (lstrcmp(szConfigName, TEXT("SettingKey")) == 0)
	{
		return g_configData.nSettingKey;
	}
	else if (lstrcmp(szConfigName,TEXT("SettingModifier")) == 0)
	{
		return g_configData.nSettingModifier;
	}

	return 0;
}