#ifndef CONFIG_H_H_H
#define CONFIG_H_H_H

#include "colorOperate.h"

/************************************************************************/
/* 类型定义		                                                        */
/************************************************************************/
typedef struct tagCONFIGDATA
{
	//取色行为配置
	UINT nView1Ratio;							//取色框1放大倍数
	UINT nView2Ratio;							//取色框2放大倍数
	COLORTYPE nColorType;						//取色类型
	BOOL bIsMove;								//取色框是否随鼠标移动
	BOOL bIsForground;							//取色框是否始终保持最前
	UINT nClrModifier;							//<取色热键>的转换
	UINT nClrKey;								//<取色热键>的键值

	//软件行为配置
	BOOL bIsHide;								//是否隐藏到后台
	BOOL bIsAutoRun;							//是否自动启动
	UINT nSettingModifier;						//<设置热键>的转换
	UINT nSettingKey;							//<设置热键>的键值
}CONFIGDATA;

/************************************************************************/
/*全局参数表                                                            */
/************************************************************************/
extern CONFIGDATA g_configData;

extern TCHAR g_szExePath[MAX_PATH] ;						//程序本身路径
extern TCHAR g_szConfigPath[MAX_PATH] ;						//软件配置文件路径

/************************************************************************/
/* 定义函数原型                                                         */
/************************************************************************/
// BOOL SetConfig(LPCTSTR szConfigFile);
// BOOL ReadConfig(LPCTSTR szConfigFile);
void ReadConfig(LPCTSTR szConfigFile);
BOOL SaveConfig(LPCTSTR szConfigFile);
BOOL SetConfig(LPCTSTR szConfigFile, LPCTSTR szConfigName, UINT nConfigValue, BOOL bSaveToFile);
UINT GetConfig(LPCTSTR szConfigName);
// BOOL AddTimeNode(const ULARGE_INTEGER time, const TIMEMODEL timeModel, const TIMEOPERATE timeOperate);
// BOOL RemoveTimeNode(const UINT nIndex);
// void ShowToListbox(const HWND hCtrl);
// int IsOnTime(const SYSTEMTIME st);
// void OperateOnTime(const int nIndex, const HWND hCtrl, LPCTSTR szConfigFile);

#endif