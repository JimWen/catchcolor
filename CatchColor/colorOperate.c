#include <Windows.h>
#include <WindowsX.h>
#include <stdio.h>

#include "colorOperate.h"
#include <tchar.h>

//全局维护的取色列表和定时个数,其作用域限制在本文件内
static COLORNODE clrList[MAX_COUNT];
static int clrCount=0;


/**
 *功能:在当前的取色历史列表添加一个取色数据,同时在显示列表中添加对应数据
 *参数:clrNode--取色数据,hList--显示列表句柄
 *返回:成功返回TRUE,否则返回FALSE
 *其他:2014/04/02 By Jim Wen Ver1.0
**/
BOOL PushClrNode(const COLORNODE clrNode, HWND hList)
{
	TCHAR szColor[20];

	//当前已经达到定时个数上限
	if (clrCount == MAX_COUNT)
	{
		return FALSE;
	}
	
	//取色列表中添加
	clrList[clrCount++] = clrNode;

	//显示列表中添加
	GetClrNodeText(clrNode, szColor);

	ListBox_InsertString(hList, 0, szColor);//最近取色最先显示
	
	return TRUE ;
}

/**
 *功能:在当前的取色历史列表添加一个取色类型和相应的数据,同时在显示列表中添加对应数据
 *参数:clrType--取色类型,clrRef--取色数据,hList--显示列表句柄
 *返回:成功返回TRUE,否则返回FALSE
 *其他:2014/04/02 By Jim Wen Ver1.0
**/
BOOL PushClrTypeAndRGB(const COLORTYPE clrType, const COLORREF clrRef, HWND hList)
{
	COLORNODE clrNode;

	clrNode.clrType = clrType;
	clrNode.byteRed = GetRValue(clrRef);
	clrNode.byteGreen = GetGValue(clrRef);
	clrNode.byteBlue = GetBValue(clrRef);

	PushClrNode(clrNode, hList);

	return TRUE;
}

/**
 *功能:清空当前取色列表并清空显示列表
 *参数:hList--显示列表句柄
 *返回:成功返回TRUE,否则返回FALSE
 *其他:2014/04/02 By Jim Wen Ver1.0
**/
BOOL ClearClrList(HWND hList)
{
	//删除数据
	clrCount = 0;

	//清空显示列表
	ListBox_ResetContent(hList);
	
	return TRUE ;
}

/**
 *功能:在当前的取色历史列表中指定序号的取色数据
 *参数:nIndex--列表序号
 *返回:取色数据
 *其他:2014/04/02 By Jim Wen Ver1.0
**/
COLORNODE GetClrNode(const UINT nIndex)
{
	return clrList[clrCount -1 - nIndex];
}

/**
 *功能:根据输入的颜色节点生成格式化的颜色字符串
 *参数:clrNode--颜色节点，szColor--颜色值字符串保存位置
 *返回:无
 *其他:2014/04/03 By Jim Wen Ver1.0
**/
void GetClrNodeText(COLORNODE clrNode, TCHAR szColor[20])
{
	switch(clrNode.clrType)
	{
	case COLORTYPE_RGB:
		wsprintf(szColor, TEXT("%03d,%03d,%03d"), clrNode.byteRed, clrNode.byteGreen, clrNode.byteBlue);
		break;

	case COLORTYPE_HTML:
		wsprintf(szColor, TEXT("#%02X%02X%02X"), clrNode.byteRed, clrNode.byteGreen, clrNode.byteBlue);
		break;

	case COLORTYPE_HEX:
		wsprintf(szColor, TEXT("%02X%02X%02X"), clrNode.byteRed, clrNode.byteGreen, clrNode.byteBlue);
		break;
	}
}

/**
 *功能:根据输入的颜色类型和颜色RGB值生成格式化的颜色字符串
 *参数:clrType--颜色类型，clrRGB--颜色RGB值，szColor--颜色值字符串保存位置
 *返回:无
 *其他:2014/04/03 By Jim Wen Ver1.0
**/
void GetClrTypeAndRGBText(COLORTYPE clrType, COLORREF clrRGB, TCHAR szColor[20])
{
	switch(clrType)
	{
	case COLORTYPE_RGB:
		wsprintf(szColor, TEXT("%03d,%03d,%03d"), GetRValue(clrRGB), GetGValue(clrRGB), GetBValue(clrRGB));
		break;

	case COLORTYPE_HTML:
		wsprintf(szColor, TEXT("#%02X%02X%02X"), GetRValue(clrRGB), GetGValue(clrRGB), GetBValue(clrRGB));
		break;

	case COLORTYPE_HEX:
		wsprintf(szColor, TEXT("%02X%02X%02X"), GetRValue(clrRGB), GetGValue(clrRGB), GetBValue(clrRGB));
		break;
	}
}

/**
 *功能:将当前颜色字符串复制到剪切板上
 *参数:hDlg--剪切板所属窗口，szClr--颜色字符串
 *返回:无
 *其他:2014/04/03 By Jim Wen Ver1.0
**/
void CopyClrToClipBoard(HWND hDlg, TCHAR szClr[20])
{
	static HGLOBAL hGlobal=NULL;
	static PTSTR pGlobal;

	hGlobal = GlobalAlloc(GHND | GMEM_SHARE, 20 * sizeof(TCHAR));
	pGlobal = GlobalLock(hGlobal);
	lstrcpy(pGlobal, szClr);
	GlobalUnlock(hGlobal);

	OpenClipboard(hDlg);
	EmptyClipboard();//同时释放前一次的hGlobal
	SetClipboardData(CF_UNICODETEXT, hGlobal);
	CloseClipboard();
}

/**
 *功能:将当前的取色历史写到指定文件中
 *参数:szClrFile--指定文件路径
 *返回:成功返回TRUE,否则返回FALSE
 *其他:2014/04/03 By Jim Wen Ver1.0
**/
BOOL ExportColorHistory(LPCTSTR szClrFile)
{
	FILE* fp ;
	int i;

	//打开文件
	if (NULL == (fp = _tfopen(szClrFile, TEXT("w"))))
	{
		return FALSE ;
	}

	//写入设置
	for (i = 0; i<clrCount; i++)
	{
		_ftprintf(fp, 
				  TEXT("TYPE:%d R:%c G:%c B:%c\n"),
				  clrList[i].clrType, 
				  clrList[i].byteRed,
				  clrList[i].byteGreen,
				  clrList[i].byteBlue);
	}

	fclose(fp) ;
	return TRUE ;
}


/**
 *功能:从当前取色历史文件中读取参数到当前的取色列表
 *参数:szClrFile--取色历史文件路径,hList--list box句柄
 *返回:成功返回TRUE,否则返回FALSE
 *其他:2014/04/03 By Jim Wen Ver1.0
**/
BOOL ImportColorHistory(LPCTSTR szClrFile, HWND hList)
{
	FILE* fp ;
	COLORNODE clrNode;

	//打开文件
	if (NULL == (fp = _tfopen(szClrFile, TEXT("r"))))
	{
		return FALSE ;
	}

	//读入设置
	ClearClrList(hList);										//先清空列表
	while( !feof(fp) )
	{
		_ftscanf(fp, 
				TEXT("TYPE:%d R:%c G:%c B:%c\n"),				//这里由于R/G/B存储均为Byte，不能用%d,否则会产生溢出
				&clrNode.clrType, 
				&clrNode.byteRed,
				&clrNode.byteGreen,
				&clrNode.byteBlue);

		PushClrNode(clrNode, hList);							//添加读入的
	}

	fclose(fp) ;
	return TRUE ;
}
